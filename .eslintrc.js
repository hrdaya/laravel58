const maxLen = 120;

module.exports = {
  root: true,
  plugins: [
    'vue',
    'prettier',
  ],
  // https://eslint.org/docs/user-guide/configuring
  env: {
    // 定義済みのグローバル変数とルールの設定
    // https://github.com/eslint/eslint/blob/master/conf/environments.js
    es6: true,
    jquery: true,
    browser: true,
    node: true
  },
  globals: {
    // グローバル変数の許可
    axios: false, // [axios](https://github.com/axios/axios)
    _: false, // [lodash](https://lodash.com/docs)
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    // ベースとなる規約の読み込み
    // https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb
    'airbnb',
    // https://github.com/vuejs/eslint-plugin-vue
    'plugin:vue/recommended',
    // https://prettier.io/docs/en/eslint.html
    'plugin:prettier/recommended',
  ],
  settings: {
    // Nuxt のパスのエイリアスの解決
    // https://github.com/leiyaoqiang/eslint-import-resolver-nuxt
    'import/resolver': {
      nuxt: {
        nuxtSrcDir: 'src',
        extensions: ['.js', '.vue'],
      },
    },
  },
  rules: {
    // 追加のルール
    'import/no-extraneous-dependencies': [
      // https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-extraneous-dependencies.md
      // devDependencies の import を許可に変更
      // airbnb のルールでエラーになるためデフォルト値に再セット
      'error', {
        devDependencies: true,
        optionalDependencies: true,
        peerDependencies: false,
      },
    ],
    'no-shadow': [
      // 親スコープ内にある変数と同じ名前の変数を宣言するのを禁止
      // vuex の中で 引数に同じ変数名を指定するので許可しておく
      'error', {
        allow: [
          'state',
          'getters',
        ]
      }
    ],
    'no-param-reassign': [
      // 関数の引数への再代入を許可するかどうか
      'error', {
        props: true,
        ignorePropertyModificationsFor: [
          'state', // vuex の state は書き込みが必要になるので許可しておく
        ],
      },
    ],
    'max-len': [
      // https://eslint.org/docs/rules/max-len
      // 一行の長さ()
      'error', {
        code: maxLen, // コードの一行の長さ(prettier の printWidth と合わせておくこと)
        ignoreComments: true, // コメントをすべて無視
        ignoreUrls: true, // URLを含む行を無視
        ignoreStrings: true, // 二重引用符または一重引用符で囲まれた文字列を含む行を無視
        ignoreTemplateLiterals: true, // テンプレートリテラルを含む行を無視
        ignoreRegExpLiterals: true, // RegExpリテラルを含む行を無視
      },
    ],
    'vue/component-name-in-template-casing': [
      // https://vuejs.github.io/eslint-plugin-vue/rules/component-name-in-template-casing.html
      // <template> 内のコンポーネントの命名スタイル
      'error',
      'kebab-case', // (PascalCase|kebab-case)
    ],
    'prettier/prettier': [
      // https://qiita.com/soarflat/items/06377f3b96964964a65d
      'error', {
        printWidth: maxLen, // 一行の長さ(max-len の code と合わせておくこと)
        singleQuote: true, // シングルクォートに変更
        trailingComma: 'all', // 末尾のカンマを出来る限り付ける
        bracketSpacing: false, // ブラケットにスペースを付けない
      },
    ],
  },
};
