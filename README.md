# Laravel 5.8

[Laravel 5.8](https://readouble.com/laravel/5.8/ja/)

- VSCode で Laravel の開発を開始するのに標準的なセットアップを済ませたもの
- 各ファイルは下記のフォーマッタにてフォーマット済み
- PHPUnit8 を使用するように変更しているので PHP7.2 以上が要件

## VSCode の設定

`.vscode` に下記のファイルを設置

- `extensions.json`
- `launch.json`
- `settings.json`

## 日本語設定

### `config/app.php` の下記の箇所の設定を変更

- `'timezone' => 'Asia/Tokyo',`
- `'locale' => 'ja',`
- `'faker_locale' => 'ja_JP',`

### 言語設定を追加

`resources/lang/ja` に下記のファイルを配置

[Laravel 5.8 auth.php言語ファイル](https://readouble.com/laravel/5.8/ja/auth-php.html) 参照

- `auth.php`
- `paginarion.php`
- `passwords.php`
- `validation.php`

### スケジューラのデフォルトのタイムゾーン指定

`app/Console/Kernel.php` にデフォルトのタイムゾーンを指定

```php
/**
 * スケジュール済みの全イベントに対するデフォルトのタイムゾーンを取得.
 *
 * @return null|\DateTimeZone|string
 */
protected function scheduleTimezone()
{
    return config('app.timezone');
}
```

## 各種 phar の配置

composer でインストールしたものだとうまく動作しないものもあるので `phar` フォルダに下記ファイルを配置

- `php-cs-fixer.phar`
  - PHP ファイルのフォーマット
  - `.vscode/settings.json` に `"php-cs-fixer.executablePath": "${workspaceRoot}\\phar\\php-cs-fixer.phar",` の記述を追加
  - フォーマットの設定は `php_cs.dist` に記述
- `phpDocumentor.phar`
  - PHP のドキュメント生成
  - 実行のコマンドは `composer.json` の `scripts` に `create-doc` として記述
  - `composer create-doc` で `documents/php/api` にファイルを生成
- `phpmd.phar`
  - 静的解析ツール
  - `.vscode/settings.json` に `"phpmd.command": "php ${workspaceRoot}\\phar\\phpmd.phar",` の記述を追加
  - 解析の設定は `phpmd.xml`

## リンター・フォーマッタの設定

### PHP のリンター

- [PHPMD](https://phpmd.org/)
  - 実行: `composer phpmd`
  - 設定: `phpmd.xml`
- [PHPStan](https://github.com/phpstan/phpstan)
  - 実行: `composer phpstan`
  - 設定: `phpstan.neon.dist`
  - [Larastan](https://github.com/nunomaduro/larastan)

まとめて実行するには `composer analyse`

### PHP のフォーマッタ

- [PHP-CS-Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer)
  - 実行: 保存時に実行される。実行されない場合は右クリックで「ドキュメントのフォーマット」を実行
  - 設定: `.php_cs.dist`

### Javascript のリンター・フォーマッタ

- [ESLint](https://eslint.org/)
  - 実行: 保存時に実行される。
  - 手動実行: `npm run fix:js`
  - 設定: `.eslintrc.js`

### Sass のリンター・フォーマッタ

- [stylelint](https://stylelint.io/)
  - 実行: `npm run fix:css`
  - 設定: `.stylelintrc.js`

## UNIT テスト

### PHP

[PHPUnit](https://phpunit.readthedocs.io/ja/latest/index.html)

使用するバージョンは `PHPUnit 8` とするため `composer.json` の記述を `"phpunit/phpunit": "^8"` に変更。

カバレージは `documents/php/coverage` に作成

- 実行: `vendor/bin/phpunit`
- 設定: `phpunit.xml`

## Clockwork

[Clockwork](https://github.com/itsgoingd/clockwork)

API の開発を行いやすくするために [Debugbar](https://github.com/barryvdh/laravel-debugbar) ではなく [Clockwork](https://github.com/itsgoingd/clockwork) を使用する。

`composer.json` に設定済み

- [Chrome extension](https://chrome.google.com/webstore/detail/clockwork/dmggabnehkmmfmdffgajcflpdjlnoemp)
- [Firefox add-on](https://addons.mozilla.org/en-US/firefox/addon/clockwork-dev-tools/)

## 使えそうなライブラリ

- [Laravel Excel](https://laravel-excel.com/)
- [Fast Excel](https://github.com/rap2hpoutre/fast-excel)
- [Goodby, CSV](https://github.com/goodby/csv)
- [Intervention Image](http://image.intervention.io/getting_started/introduction)
- [laravel-medialibrary](https://docs.spatie.be/laravel-medialibrary/v7/introduction)
- [HTMLPurifier for Laravel 5](https://github.com/mewebstudio/Purifier)
- [fpdi-tcpdf](https://github.com/Setasign/FPDI-TCPDF)
- [laravel-permission](https://github.com/spatie/laravel-permission)
- [jwt-auth](https://github.com/tymondesigns/jwt-auth)
