module.exports = {
  plugins: [
    'stylelint-scss',
    'stylelint-selector-no-empty',
    'stylelint-prettier',
  ],
  extends: [
    'stylelint-config-standard',
    'stylelint-config-property-sort-order-smacss',
  ],
  ignoreFiles: ['**/node_modules/**', '**/dist/**'],
  rules: {
    // scssを使うには↓の2つがないと@mixinとかでエラーになってしまう。
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': true,
    // https://www.npmjs.com/package/stylelint-selector-no-empty
    'plugin/stylelint-selector-no-empty': true,
    // シングルクォート
    'string-quotes': 'single',
  },
};
